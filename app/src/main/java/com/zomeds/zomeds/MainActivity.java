package com.zomeds.zomeds;

import android.os.Bundle;
import android.os.Build;
import android.graphics.Bitmap;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends AppCompatActivity {

    private WebView wv1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ProgressDialog pd = ProgressDialog.show(MainActivity.this, null, "Loading Zomeds, please wait....", true);
        setContentView(R.layout.activity_main);

        wv1=(WebView)findViewById(R.id.webView);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // chromium, enable hardware acceleration
            wv1.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            // older android version, disable hardware acceleration
            wv1.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setDomStorageEnabled(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon)
            {
                pd.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
                    String webUrl = wv1.getUrl();

            }

        });
        wv1.loadUrl("https://app.zomeds.com");
    }

}
